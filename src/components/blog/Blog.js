import React from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { addBlogPost } from "../../actions";
import BlogInfo from "./BlogInfo";
import VisibleBlog from "./VisibleBlog";
import BlogForm from "./BlogForm";


const Blog = ({ addBlogPost, match: { params }, history }) => (
    <div className="blog">
        <div className="secondary__block">
            <BlogForm addPost={addBlogPost} history={history}/>
            <BlogInfo/>
        </div>

        <VisibleBlog filter={params.filter}/>
    </div>
);

export default withRouter(connect(
    null,
    { addBlogPost }
)(Blog));
