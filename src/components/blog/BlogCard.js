import React from 'react';
import { Icon } from 'react-icons-kit';
import { ic_close } from 'react-icons-kit/md/ic_close';
import { ic_edit } from 'react-icons-kit/md/ic_edit';
import { Link } from "react-router-dom";

const BlogCard = ({ id, title, description, topic, createdAt, removePost }) => {
    const blogRestiction = description.substr(0, 60);

    const topicName = () => {
        switch (topic) {
            case 'grammar':
                return <p>GR</p>;
            case 'phrasal':
                return <p>PV</p>;
            case 'it':
                return <p>IT</p>;
            case 'other':
                return <p>OT</p>;
            default:
                return null;
        }
    };

    return (
        <div className="blog-card">
            <div className="main__block">
                <div className="blog-card__topic">
                    {topicName()}
                </div>

                <div>
                    <h4 className="blog-card__title">{title}</h4>
                    {description.length > 60 ? `${blogRestiction}...` : <p>{description}</p>}
                </div>
            </div>
            <div className="main__block">
                <time>{createdAt}</time>
                <Link to={`/blog/edit/${id}`} className="blog-card__btn edit"><Icon icon={ic_edit} size={24}/></Link>
                <button className="blog-card__btn remove" onClick={() => removePost(id)}>
                    <Icon icon={ic_close} size={24}/>
                </button>
            </div>
        </div>
    )
};

export default BlogCard;