import React from 'react';
import { connect } from "react-redux";
import { getVisiblePosts } from "../filter/getVisiblePosts";
import { getBlogPosts, removeBlogPost } from "../../actions";
import BlogCard from "./BlogCard";
import BlogFilter from "../filter/BlogFilter";

class VisibleBlog extends React.Component {
    componentDidMount() {
        this.props.getBlogPosts(this.props.filter);
    }

    componentDidUpdate(prevProps) {
        if (this.props.filter !== prevProps.filter) {
            this.props.getBlogPosts(this.props.filter);
        }
    }

    render() {
        const {filteredBlog, removeBlogPost} = this.props;
        return (
            <div>
                <BlogFilter/>
                {filteredBlog.map(item => (
                        <BlogCard
                            removePost={removeBlogPost}
                            key={item.id}
                            {...item}
                        />
                    )
                )}
            </div>
        )
    }
}

const mapStateToProps = ({blog}, ownProps) => {
    const filter = ownProps.filter || 'all';
    return {
        filteredBlog: getVisiblePosts(blog, filter),
        filter
    }
};

export default connect(
    mapStateToProps,
    {getBlogPosts, removeBlogPost}
)(VisibleBlog);
