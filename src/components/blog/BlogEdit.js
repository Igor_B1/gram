import React from 'react';
import { withFormik, Form, Field } from 'formik';
import * as Yup from 'yup'
import { connect } from "react-redux";
import moment from "moment";
import { renderInput, renderTextarea } from "../renderElements";
import { editBlogPost } from "../../actions";


const BlogEdit = ({ isSubmitting }) => (
    <div className="blog-edit__block">
        <h4 className="title">Edit post</h4>
        <Form className="blog-edit__form">
            <Field
                component={renderInput}
                name="title"
                placeholder="title"
                label="Title"
            />
            <Field
                component={renderTextarea}
                name="description"
                placeholder="description"
                label="Description"
            />
            <Field name="topic" component="select" className="select" label="Choose topic">
                <option value="default">Choose your topic</option>
                <option value="grammar">Grammar</option>
                <option value="phrasal">Phrasal Verbs</option>
                <option value="it">IT English</option>
            </Field>
            <button disabled={isSubmitting} type="submit" className="dictionary__btn">
                Submit
            </button>
        </Form>
    </div>
);

const FormikEnchancer = withFormik({
    mapPropsToValues({title, description, topic, createdAt, match}) {
        return {
            id: match.params.id,
            title: title || '',
            description: description || '',
            topic: topic || 'default',
            createdAt: moment().format("MMM Do YY")
        }
    },
    validationSchema: Yup.object().shape({
        id: Yup.string().required(),
        title: Yup.string().required(),
        description: Yup.string().required(),
        topic: Yup.string().required()
    }),

    handleSubmit(values, {setSubmitting, resetForm, props}) {
        props.editBlogPost(values, values.id);
        setSubmitting(false);
        resetForm();
        props.history.push('/blog')
    }
})(BlogEdit);


export default connect(
    null,
    { editBlogPost }
)(FormikEnchancer);