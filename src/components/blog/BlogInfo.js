import React from 'react';

const BlogInfo = () => (
    <section>
        <h4>Transcript</h4>
        <hr className="blog__hr"/>
        <ul className="blog__info">
            <li><b>GR</b><span> - Grammar</span></li>
            <li><b>IT</b><span> - IT English</span></li>
            <li><b>PV</b><span> - Phrasal Verbs</span></li>
            <li><b>OT</b><span> - Other topic</span></li>
        </ul>
        <hr className="blog__hr"/>
        <h4>Hints: </h4>
        <ul className="blog__info">
            <li>Clear explanation of the problem increases your chances to get the right solution.</li>
        </ul>
    </section>
);

export default BlogInfo;