import React from 'react';
import { withFormik, Form, Field } from 'formik';
import * as Yup from 'yup'
import uuid from 'uuid/v4';
import { renderInput, renderTextarea } from "../renderElements";

const BlogForm = ({ isSubmitting }) => (
    <Form>
        <h4>Add your post here</h4>
        <hr className="blog__hr"/>
        <div>
            <span>Title</span>
            <Field
                component={renderInput}
                name="title"
                placeholder="title"
            />
        </div>
        <div>
            <span>Description</span>
            <Field
                component={renderTextarea}
                name="description"
                placeholder="description"
            />
        </div>
        <div>
            <span>Choose topic</span>
            <Field name="topic" component="select" className="select">
                <option value="other">Other topic</option>
                <option value="grammar">Grammar</option>
                <option value="phrasal">Phrasal Verbs</option>
                <option value="it">IT English</option>
            </Field>
        </div>

        <button disabled={isSubmitting} type="submit" className="blog__btn">
            Submit
        </button>
    </Form>
);

export default withFormik({
    mapPropsToValues({title, description, topic, createdAt}) {
        return {
            id: uuid(),
            title: title || '',
            description: description || '',
            topic: topic || 'other',
            createdAt: createdAt || ''
        }
    },
    validationSchema: Yup.object().shape({
        id: Yup.string().required(),
        title: Yup.string().required(),
        description: Yup.string().required(),
        topic: Yup.string().required()
    }),

    handleSubmit(values, {setSubmitting, resetForm, props}) {
        props.addPost(values);
        setSubmitting(false);
        resetForm();
    }
})(BlogForm);

