import React from 'react';
import { Link } from "react-router-dom";
import Fade from 'react-reveal/Fade';

const Dashboard = () => (
    <div className="dashboard">
        <Fade top big cascade>
            <h1 className="dashboard__title">Gram</h1>
        </Fade>
        <Fade bottom>
            <h4 className="dashboard__subtitle">The best place to improve your English</h4>
            <Link to={"/grammar"} className="dashboard__link">Get Started</Link>
        </Fade>
    </div>
);

export default Dashboard;