import React from 'react';

export const renderInput = ({
        field,
        form: {touched, errors},
        ...props
    }) => (
        <div>
            <label className="label">{props.label}</label>
            <input
                type="text"
                name={field.name}
                className="input"
                {...field}
                {...props}
            />
            <p>{touched.text && errors.text && <span>{errors.text}</span>}</p>
        </div>

);

export const renderTextarea = ({
    field,
    form: {touched, errors},
    ...props
}) => (
    <div>
        <label className="label">{props.label}</label>
        <textarea
            name={field.name}
            className="input"
            {...field}
            {...props}
            cols="30"
            rows="4"
        >

        </textarea>
        {touched.text && errors.text && <p>{errors.text}</p>}
    </div>
);