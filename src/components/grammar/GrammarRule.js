import React from 'react';
import { connect } from "react-redux";
import { getGrammar } from "../../actions";
import GrammarDefinitionMain from './GrammarDefinitionMain';
import GrammarDefinition from './GrammarDefinition';


class GrammarRule extends React.Component {
    componentDidMount() {
        const { level } = this.props.match.params;
        this.props.getGrammar(level);
    }

    render() {
        const { rules, definitions } = this.props;
        return (
            <div className="grammar__block">
                { rules.map(rule => <GrammarDefinitionMain key={rule.id} {...rule}/>) }
                { definitions.map(def => <GrammarDefinition key={def.id} {...def}/>) }
            </div>
        );
    }
}

const mapStateToProps = ({ grammar }, ownProps) => {
    const { rules, definitions } = grammar.byId;
    const { id } = ownProps.match.params;
    return {
        rules: Object.keys(rules || {}).map(key => rules[key]).filter(item => item.id === +id),
        definitions: Object.keys(definitions || {}).map(key => definitions[key]).filter(item => item.ruleId === +id)
    }
};

export default connect(
    mapStateToProps,
    { getGrammar }
)(GrammarRule);