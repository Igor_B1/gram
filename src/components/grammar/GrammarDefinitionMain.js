import React from 'react';

const GrammarDefinitionMain = ({ id, title, description }) => (
    <div className="gram-definition__main">
        <h4 className="title">{title}</h4>
        <p>{description}</p>
    </div>
);

export default GrammarDefinitionMain;