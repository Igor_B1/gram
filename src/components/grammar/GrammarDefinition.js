import React, { Fragment } from 'react';
import Fade from 'react-reveal/Fade';

const GrammarDefinition = ({ usage, term, examples }) => (
    <Fragment>
        <Fade>
            <div className="gram-definition">
                <h4 className="title">{term}</h4>
                <p>{usage}</p>
                <hr className="grammar__hr"/>
                <div>
                    <strong>Examples: </strong>
                    { examples.map((ex, i) => <li key={i}>{ex}</li>) }
                </div>
            </div>
        </Fade>
    </Fragment>
);

export default GrammarDefinition;