import React from 'react';
import { Link } from 'react-router-dom';
import { Icon } from 'react-icons-kit';
import { angleDoubleRight } from 'react-icons-kit/fa/angleDoubleRight';

const GrammarCard = ({ id, title, description, complexity, usage, level }) => {
    const titleRest = title.substr(0, 30);
    const descRest = description.substr(0, 60);
    return (
        <div className="gram-card">
            <div className="gram-card__text">
                <div className="tooltip">
                    <h5 className="gram-card__title">
                        {title.length > 30 ?
                            <p>{titleRest}... <span className="tooltip__text">{title}</span></p>
                            : title}
                    </h5>
                </div>
                <p className="gram-card__description">{description.length > 60 ?
                    <div>{descRest}... </div> : description}</p>
                <Link className="gram-card__link" to={`/grammar/${level}/${id}`}>
                    <span>Learn</span>
                    <Icon icon={angleDoubleRight}/>
                </Link>
                <div className="gram-card__complexity"><span>Complexity:</span> {complexity}</div>
            </div>
        </div>
    );
};

export default GrammarCard;