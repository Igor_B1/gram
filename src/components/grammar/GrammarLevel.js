import React, { Fragment } from 'react';
import { connect } from "react-redux";
import { getGrammar } from "../../actions";
import { withRouter } from "react-router-dom";
import GrammarCard from './GrammarCard';


class GrammarLevel extends React.Component {
    componentDidMount() {
        this.props.getGrammar(this.props.filter);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.filter !== prevProps.filter) {
            this.props.getGrammar(this.props.filter);
        }
    }

    render() {
        const { rules, filter } = this.props;
        return (
            <Fragment>
                {
                    filter !== undefined ?
                    <section className="main__block">
                        {rules.map(rule => <GrammarCard key={rule.id} {...rule} level={filter}/>)}
                    </section> : ''
                }
            </Fragment>
        )
    }
}

const mapStateToProps = ({ grammar }, ownProps) => {
    const { filter } = ownProps.match.params;
    const { rules } = grammar.byId;
    return {
        rules: Object.keys(rules || {}).map(key => rules[key]),
        filter
    }
};

export default withRouter(connect(
    mapStateToProps,
    { getGrammar }
)(GrammarLevel));