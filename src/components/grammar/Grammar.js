import React from 'react';
import GrammarFilter from "../filter/GrammarFilter";
import GrammarLevel from "./GrammarLevel";

const Grammar = () => (
    <main className="grammar">
        <h4 className="title">Choose level:</h4>
        <GrammarFilter/>
        <GrammarLevel/>
    </main>
);

export default Grammar;


