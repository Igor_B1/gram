import React from 'react';
import GrammarFilterLink from "./GrammarFilterLink";

const GrammarFilter = () => (
    <div className="filter-group" role="group">
        <GrammarFilterLink filter="/grammar/beginner">Beginner</GrammarFilterLink>
        <GrammarFilterLink filter="/grammar/intermediate">Intermediate</GrammarFilterLink>
        <GrammarFilterLink filter="/grammar/advanced">Advanced</GrammarFilterLink>
    </div>
);

export default GrammarFilter;



