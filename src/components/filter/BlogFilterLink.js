import React from 'react';
import { NavLink } from "react-router-dom";

const BlogFilterLink = ({ filter, children }) => (
    <NavLink
        to={filter === '/blog/all' ? '/blog/all' : filter}
        className="filter__btn"
        activeStyle={{ backgroundColor: 'teal' }}
    >
        {children}
    </NavLink>
);

export default BlogFilterLink;
