export const getVisiblePosts = (state, filter) => {
    const ids = state.idsByFilter[filter];
    return ids.map(id => state.byId[id]).filter(item => item !== undefined);
};
