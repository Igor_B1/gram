import React from 'react';
import { NavLink } from "react-router-dom";

const GrammarFilterLink = ({ filter, children }) => (
    <NavLink
        to={filter === 'undefined' ? '/grammar/beginner' : filter}
        className="filter__btn"
    >
        { children }
    </NavLink>
);

export default GrammarFilterLink;