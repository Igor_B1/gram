import React from 'react';
import BlogFilterLink from './BlogFilterLink';

const BlogFilter = () => (
    <div className="filter-group" role="group">
        <BlogFilterLink filter="/blog/all">All posts</BlogFilterLink>
        <BlogFilterLink filter="/blog/grammar">Grammar</BlogFilterLink>
        <BlogFilterLink filter="/blog/it">IT English</BlogFilterLink>
        <BlogFilterLink filter="/blog/phrasalVerbs">Phrasal Verbs</BlogFilterLink>
    </div>
);

export default BlogFilter;