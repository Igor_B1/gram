import React from 'react';

const NotFound = () => <div className="main__block"><b>Page not found!</b></div>;

export default NotFound;