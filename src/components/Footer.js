import React, { Fragment } from 'react';
import { facebook2 } from 'react-icons-kit/icomoon/facebook2';
import { telegram } from 'react-icons-kit/icomoon/telegram';
import { youtube } from 'react-icons-kit/icomoon/youtube';
import { instagram } from 'react-icons-kit/icomoon/instagram';
import { Icon } from "react-icons-kit";
import Fade from "react-reveal/Fade";


const Footer = () => (
    <Fragment>
        <Fade bottom>
            <footer className="footer">
                <Fade bottom delay={800}>
                    <Icon icon={ facebook2 } size={24}/>
                    <Icon icon={ telegram } size={24}/>
                    <Icon icon={ youtube } size={24}/>
                    <Icon icon={ instagram } size={24}/>
                </Fade>
            </footer>
        </Fade>
    </Fragment>
);

export default Footer;