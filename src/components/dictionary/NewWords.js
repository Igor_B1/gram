import React, { Fragment } from 'react';
import { Icon } from "react-icons-kit";
import { ic_expand_more } from 'react-icons-kit/md/ic_expand_more';
import DictionaryWord from "./DictionaryWord";


const NewWords = ({ words }) => (
    <Fragment>
        <h5 className="title">New words</h5>
            {words.length < 1 ?
                <div className="dictionary__block--empty">
                    <p>Your new words will be added here... Drag word that you've already learned and drop it inside 'Learned words' container.</p>
                    <Icon icon={ic_expand_more} size={32} className="dictionary__icon"/>
                </div> :
                <div className="dictionary__block">
                    { words.map(w => <DictionaryWord key={w.id} {...w}/>) }
                </div>
            }
    </Fragment>

);

export default NewWords;