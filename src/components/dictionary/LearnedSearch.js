import React from 'react';
import Fade from "react-reveal/Fade";

const LearnedSearch = ({ search, searchLearnedWord }) => (
    <Fade bottom>
        <input
            type="text"
            className="input"
            placeholder="Search word..."
            value={search}
            onChange={searchLearnedWord}
        />
    </Fade>
);

export default LearnedSearch;