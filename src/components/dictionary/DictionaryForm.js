import React from 'react';
import { Field, Form, withFormik } from "formik";
import uuid from "uuid/v4";
import * as Yup from "yup";
import { renderInput } from "../renderElements";


const DictForm = ({ isSubmitting }) => (
    <Form className="dictionary__form">
        <Field
            name="word"
            component={ renderInput }
            placeholder="word"
            label="Add word"
        />
        <Field
            name="translation"
            component={ renderInput }
            placeholder="translation"
            label="Add translation"
        />
        <button type="submit" disabled={isSubmitting} className="dictionary__btn">Add word</button>
    </Form>
);

const DictionaryForm = withFormik({
    mapPropsToValues({ word, translation }) {
        return {
            id: uuid(),
            word: word || '',
            translation: translation || ''
        }
    },
    validationSchema: Yup.object().shape({
        id: Yup.string().required(),
        word: Yup.string().required('Please add a translation'),
        translation: Yup.string().required('Please add a translation'),
    }),

    handleSubmit( values, { setSubmitting, resetForm, props }) {
        props.addWord(values);
        setSubmitting(false);
        resetForm();
    }
})(DictForm);

export default DictionaryForm;