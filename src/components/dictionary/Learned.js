import React from 'react';
import { ic_done_all } from 'react-icons-kit/md/ic_done_all';
import { ic_close } from 'react-icons-kit/md/ic_close';
import { Icon } from "react-icons-kit";
import { connect } from "react-redux";
import { deleteWordFromLearned } from "../../actions";


const Learned = ({ id, word, translation, deleteWordFromLearned }) => (
    <div className="word">
        <div className="word__info">
            <span>{word}</span>
            <span>{translation}</span>
        </div>
        <div>
            <Icon icon={ic_done_all} size={20}/>
            <Icon icon={ic_close} size={20} onClick={() => deleteWordFromLearned(id)}/>
        </div>
    </div>
);

export default connect(null, { deleteWordFromLearned })(Learned);