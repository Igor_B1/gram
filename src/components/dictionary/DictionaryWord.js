import React from 'react';
import {ic_done} from 'react-icons-kit/md/ic_done';
import {Icon} from "react-icons-kit";
import {DragSource} from "react-dnd";

const wordSource = {
    beginDrag: props => props
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    }
}

const DictionaryWord = ({ translation, id, word, connectDragSource }) => {
    return connectDragSource(
        <div className="word">
            <div className="word__info">
                <span>{word}</span>
                <span>{translation}</span>
            </div>
            <Icon icon={ic_done}/>
        </div>
    );
};

export default DragSource('word', wordSource, collect)(DictionaryWord);