import React from 'react';
import {
    addWord,
    getWord,
    clearWord,
    getLearned,
    addToLearned,
    deleteWordFromDictionary,
    clearLearnedWord
} from "../../actions";
import { connect } from "react-redux";
import TouchBackend from 'react-dnd-touch-backend';
import { DragDropContext } from 'react-dnd';
import DictionaryForm from './DictionaryForm';
import NewWords from "./NewWords";
import LearnedWords from "./LearnedWords";
import LearnedSearch from "./LearnedSearch";



class Dictionary extends React.Component {
    state = { search: '' };

    componentDidMount() {
        this.props.getWord();
        this.props.getLearned();
    }

    componentWillUnmount() {
        this.props.clearWord();
        this.props.clearLearnedWord();
    }

    searchLearnedWord = (e) => this.setState({ search: e.target.value });

    addToLearnedWords = (item) => {
        const { addToLearned, deleteWordFromDictionary } = this.props;
        addToLearned(item);
        deleteWordFromDictionary(item.id);
    };

    render() {
        const { dictionary, learned, addWord } = this.props;
        const { search } = this.state;
        const filteredLearned = learned.filter(i => i.word.toLowerCase().includes(search.toLowerCase()));
        return (
            <div className="dictionary">
                <h4 className="title">Dictionary</h4>
                <DictionaryForm addWord={addWord}/>
                <NewWords words={dictionary} />

                <h5 className="title">Learned words</h5>
                {
                    learned.length < 2 ? '' :
                    <LearnedSearch
                        search={search}
                        searchLearnedWord={this.searchLearnedWord}
                    />
                }
                <LearnedWords
                    learned={filteredLearned}
                    addToLearnedWords={this.addToLearnedWords}
                />
            </div>
        )
    }
}

Dictionary = DragDropContext(TouchBackend({
    enableMouseEvents: true,
    enableKeyboardEvents: true
}))(Dictionary);

export default connect(
    ({ dictionary, learned }) => ({ dictionary, learned }),
    {
        addWord,
        getWord,
        clearWord,
        getLearned,
        addToLearned,
        deleteWordFromDictionary,
        clearLearnedWord
    })
(Dictionary);