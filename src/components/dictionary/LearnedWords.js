import React from 'react';
import Learned from "./Learned";
import { DropTarget } from "react-dnd";

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget()
    };
}

const LearnedWords = ({ learned, connectDropTarget }) => {
    return connectDropTarget(
        <div className="dictionary__block">
            {learned.length < 1 ? <p>Drag your word from "newWords" and drop it right here if you finish learning it...</p> :
                learned.map(t => <Learned {...t} key={t.id}/>)}
        </div>
    )
};


export default DropTarget('word', {
    drop: (props, monitor) => {
        const { id, word, translation } = monitor.getItem();
        const sentWord = { id, word, translation };
        props.addToLearnedWords(sentWord);
    }
}, collect)(LearnedWords);