import React from 'react';
import { NavLink } from "react-router-dom";

const HeaderLink = ({ children, ...rest }) => (
    <NavLink activeClassName="header__link--active" {...rest}>
        {children}
    </NavLink>
);

export default HeaderLink;
