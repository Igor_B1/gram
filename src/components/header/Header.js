import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from "react-router-dom";
import { Icon } from "react-icons-kit";
import { ic_menu } from 'react-icons-kit/md/ic_menu';
import { ic_close } from 'react-icons-kit/md/ic_close';
import HeaderLink from './HeaderLink';
import Sidebar from "./Sidebar";
import Backdrop from "./Backdrop";


class Header extends React.Component {
    state = {
        isOpen: false
    };

    toggleSidebar = () => {
        this.setState({ isOpen: !this.state.isOpen })
    };

    closeSidebar = () => {
        this.setState({ isOpen: false })
    };

    render() {
        const { pathname } = this.props.location;
        let backdrop;

        if (this.state.isOpen) {
            backdrop = <Backdrop click={this.closeSidebar}/>
        }

        return (
            <header className={pathname === '/' ? 'header' : 'header-secondary'}>
                <Link className="header__logo" to="/">Gram</Link>
                {!this.state.isOpen ?
                    <Icon icon={ic_menu} size={24} className="header__toggler" onClick={this.toggleSidebar}/> :
                    <Icon icon={ic_close} size={24} className="header__toggler"/>}
                {this.state.isOpen &&
                <Fragment>
                    <Sidebar pathname={pathname}/>
                    {backdrop}
                </Fragment>}
                <nav className="header__nav">
                    <HeaderLink className="header__link" to={'/grammar'}>Grammar</HeaderLink>
                    <HeaderLink className="header__link" to={'/dictionary'}>Dictionary</HeaderLink>
                    <HeaderLink className="header__link" to={'/blog'}>Blog</HeaderLink>
                </nav>
            </header>
        );
    }
}

export default withRouter(Header);