import React from 'react';

const Backdrop = ({ click }) => (
    <div onClick={click} className="backdrop">

    </div>
);

export default Backdrop;