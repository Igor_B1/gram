import React from 'react';
import HeaderLink from "./HeaderLink";

const Sidebar = ({ pathname }) => (
    <nav className={pathname === '/' ? 'dashboard__sidebar' : 'sidebar'}>
        <HeaderLink className="sidebar__link" to={'/'}>Dashboard</HeaderLink>
        <HeaderLink className="sidebar__link" to={'/grammar'}>Grammar</HeaderLink>
        <HeaderLink className="sidebar__link" to={'/dictionary'}>Dictionary</HeaderLink>
        <HeaderLink className="sidebar__link" to={'/blog'}>Blog</HeaderLink>
    </nav>
);

export default Sidebar;