import { DICTIONARY } from "../../actions/types";
import dictionaryReducer from '../../reducers/dictionaryReducer';
import { dictionary } from "../fixtures/dictionary";

test('should set the initial state', () => {
    const state = dictionaryReducer(undefined, {type: '@@INIT'});
    expect(state).toEqual([]);
});


test('should add word to learned', () => {
    const word = {
        id: 3,
        title: 'word',
        description: 'слово'
    };
    const action = {
        type: DICTIONARY.ADD_WORD,
        payload: word
    };
    const state = dictionaryReducer(dictionary, action);
    expect(state).toEqual([...dictionary, word]);
});


test('should remove word from dictionary', () => {
    const action = {
        type: DICTIONARY.REMOVE_WORD,
        id: dictionary[0].id
    };

    const state = dictionaryReducer(dictionary, action);
    expect(state).toEqual([dictionary[1]])

});
