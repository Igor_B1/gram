import {
    getWord,
    addWord,
    deleteWordFromDictionary,
    deleteWordFromLearned,
    getLearned,
    clearLearnedWord,
    clearWord
} from "../../actions";
import thunk from 'redux-thunk';
import moxios from 'moxios';
import configureMockStore from 'redux-mock-store';
import {DICTIONARY, LEARNED} from "../../actions/types";
// import {learned} from "../fixtures/learned";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


const learned = [
    {
        "id": 1,
        "word": "car",
        "translation": "автомобиль"
    },
    {
        "id": 2,
        "word": "play",
        "translation": "играть"
    }
];
//
// describe('dictionary actions', () => {
//     beforeEach(() => {
//         moxios.install();
//         moxios.stubRequest('http://localhost:3004/learned', {
//             status: 200,
//             response: learned
//         })
//     });
//     afterEach(() => {
//         moxios.uninstall()
//     });
//
//     test('store is updated correctly', () => {
//         moxios.wait(() => {
//             const request = moxios.requests.mostRecent();
//             request.respondWith({
//                 status: 200,
//                 response: learned
//             })
//         });
//
//         const store = mockStore();
//
//
//         store.dispatch(getLearned());
//         expect(store.getState()).toEqual(learned)
//
//     })
// });


test('should setup clear word action object', () => {
    const action = clearWord();
    expect(action).toEqual({
        type: DICTIONARY.CLEAR_WORD,
        payload: []
    })
});

test('should setup clear learned word action object', () => {
    const action = clearLearnedWord();
    expect(action).toEqual({
        type: LEARNED.CLEAR_WORD,
        payload: []
    })
});