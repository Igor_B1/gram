import React from 'react';
import { shallow } from 'enzyme';
import Dashboard from "../../components/Dashboard";
import { Link } from "react-router-dom";

test('Dashboard component should render a single link', () => {
    const wrapped = shallow(<Dashboard/>);
    expect(wrapped.find(Link).length).toBe(1);
});