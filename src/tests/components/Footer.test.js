import React from 'react';
import {shallow} from 'enzyme';
import {Icon} from "react-icons-kit";
import Footer from '../../components/Footer';



describe('footer tests', () => {
   let wrapped;

   beforeEach(() => {
      wrapped = shallow(<Footer/>);
   });

   test('should contain 4 social icons', () => {
      expect(wrapped.find(Icon).length).toBe(4);
   });

   test('should match snapshot', () => {
      expect(wrapped).toMatchSnapshot();
   });
});
