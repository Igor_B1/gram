import React from 'react';
import Sidebar from '../../components/header/Sidebar';
import HeaderLink from '../../components/header/HeaderLink';
import {shallow} from 'enzyme';


describe('sidebar tests', () => {
    let wrapped;

    beforeEach(() => {
        wrapped = shallow(<Sidebar/>);
    });

    afterEach(() => {
        wrapped.unmount();
    });

    test('should render ', () => {
        expect(wrapped.find(HeaderLink).length).toBe(4);
    });

    test('should match snapshot', () => {
        expect(wrapped).toMatchSnapshot();
    });

});

