import thunk from "redux-thunk";
import { applyMiddleware, createStore } from "redux";
import reducers from '../../reducers/index';
const middlewares = [thunk];

export const testStore = (initialState) => {
    const createStoreWithMiddleWare = applyMiddleware(...middlewares)(createStore);
    return createStoreWithMiddleWare(reducers, initialState);
};