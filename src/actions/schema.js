import { schema } from "normalizr";


//Blog
const post = new schema.Entity('post');
const blogSchema = new schema.Entity('blog', {
    posts: [post]
});

const blog = [blogSchema];



//Grammar
const definitions = new schema.Entity('definitions');
const rules = new schema.Entity('rules', {
    definitions: [definitions]
});

const grammarSchema = new schema.Entity('grammar', {
    rules: [rules],
    definitions: [definitions]
});

const grammar = [grammarSchema];

export {blog, grammar, post};