import { GRAMMAR, BLOG, DICTIONARY, LEARNED } from './types';
import { normalize } from "normalizr";
import { blog, grammar, post } from "./schema";
import axios from 'axios';
import uuid from 'uuid/v4';
import moment from "moment";

const URL = 'http://localhost:3004';


//grammar
const getGrammar = filter => dispatch => {
    axios.get(`${URL}/grammar`)
        .then(res => {
            switch (filter) {
                case "beginner":
                    return res.data.filter(i => i.level === 'beginner');
                case "intermediate":
                    return res.data.filter(i => i.level === 'intermediate');
                case "advanced":
                    return res.data.filter(i => i.level === 'advanced');
                default:
                    return res.data;
            }
        })
        .then(data => {
            dispatch({
                type: GRAMMAR.GET,
                filter,
                payload: normalize(data, grammar)
            })
        })
        .catch(error => console.log(error))
};

//dictionary
const getWord = () => dispatch => {
    axios.get(`${URL}/dictionary`)
        .then(res => {
            dispatch({
                type: DICTIONARY.GET,
                payload: res.data
            })
        })
        .catch(error => console.log(error))
};


const addWord = ({ word, translation, topic }) => dispatch => {
    axios.post(`${URL}/dictionary`, {
        id: uuid(),
        word,
        translation,
        topic
    })
        .then(response => {
            dispatch({
                type: DICTIONARY.ADD_WORD,
                payload: response.data
            })
        })
        .catch(error => console.log(error))
};

const deleteWordFromDictionary = id => dispatch => {
    axios.delete(`${URL}/dictionary/${id}`)
        .then(res => {
            dispatch({
                type: DICTIONARY.REMOVE_WORD,
                id
            })
        })
        .catch(error => console.error(error));
};


const getLearned = () => dispatch => {
    axios.get(`${URL}/learned`)
        .then(res => {
            dispatch({
                type: LEARNED.GET,
                payload: res.data
            })
        })
        .catch(error => console.error(error))
};

const addToLearned = ({ id, word, translation }) => dispatch => {
    axios.post(`${URL}/learned`, {
        id,
        word,
        translation
    })
        .then(res => {
            dispatch({
                type: LEARNED.ADD,
                payload: res.data
            })
        })
        .catch(error => console.error(error))
};

const deleteWordFromLearned = id => dispatch => {
    axios.delete(`${URL}/learned/${id}`)
        .then(res => {
            dispatch({
                type: LEARNED.REMOVE_WORD,
                id
            })
        })
        .catch(error => console.error(error))
};


const clearWord = () => ({ type: DICTIONARY.CLEAR_WORD, payload: [] });
const clearLearnedWord = () => ({ type: LEARNED.CLEAR_WORD, payload: [] });


//blog
const getBlogPosts = filter => dispatch => {
    axios.get(`${URL}/blog`)
        .then(res => {
            switch (filter) {
                case "all":
                    return res.data;
                case "phrasalVerbs":
                    return res.data.filter(p => p.topic === 'phrasal');
                case "grammar":
                    return res.data.filter(p => p.topic === 'grammar');
                case "it":
                    return res.data.filter(p => p.topic === 'it');
                default:
                    return res.data;
            }
        })
        .then(data => {
            dispatch({
                type: BLOG.GET,
                filter,
                payload: normalize(data, blog)
            })
        })
        .catch(error => console.error(error))
};

const addBlogPost = ({ title, description, topic }) => dispatch => {
    axios.post(`${URL}/blog`, {
        id: uuid(),
        title,
        description,
        topic,
        createdAt: moment().format("MMM Do YY")
    })
        .then(res => {
            dispatch({
                type: BLOG.ADD,
                payload: normalize(res.data, post)
            });
        })
        .catch(error => console.error(error))
};


const editBlogPost = ( updates, id ) => dispatch => {
    axios.put(`${URL}/blog/${id}`, updates)
        .then(res => {
            dispatch({
                type: BLOG.EDIT,
                id,
                updates
            })
        })
        .catch(error => console.error(error))
};

const removeBlogPost = id => dispatch => {
    axios.delete(`${URL}/blog/${id}`)
        .then(res => {
            dispatch({
                type: BLOG.REMOVE,
                id
            })
        })
        .catch(error => console.error(error))
};

export {
    URL,
    getGrammar,
    getWord,
    getLearned,
    getBlogPosts,
    addBlogPost,
    addWord,
    addToLearned,
    clearLearnedWord,
    clearWord,
    deleteWordFromLearned,
    deleteWordFromDictionary,
    removeBlogPost,
    editBlogPost
};