import { combineReducers } from 'redux';
import grammar from './grammarReducer';
import blog from './blogReducer';
import dictionary from './dictionaryReducer';
import learned from './learnedReducer';

const reducers = combineReducers({
    grammar,
    blog,
    dictionary,
    learned
});

export default reducers;