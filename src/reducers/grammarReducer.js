import { GRAMMAR } from '../actions/types';
import { combineReducers } from "redux";
import { createList } from "./createList";

const byId = (state = {}, action) => {
    switch (action.type) {
        case GRAMMAR.GET:
            return {...state, ...action.payload.entities};
        case GRAMMAR.GET_RULE:
            return {...state, ...action.payload.entities};
        default:
            return state;
    }
};

const idsByFilter = combineReducers({
    beginner: createList('beginner'),
    intermediate: createList('intermediate'),
    advanced: createList('advanced')
});

const grammar = combineReducers({
    byId,
    idsByFilter
});


export default grammar;
