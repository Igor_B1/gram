import { BLOG } from "../actions/types";
import { combineReducers } from "redux";
import { createList } from "./createList";
import omit from 'lodash/omit';

const byId = (state = {}, action) => {
    switch (action.type) {
        case BLOG.GET:
            return {...state, ...action.payload.entities.blog};
        case BLOG.EDIT:
            return {...state, [action.id]: action.updates};
        case BLOG.REMOVE:
            return omit(state, action.id);
        default:
            return state;
    }
};

const idsByFilter = combineReducers({
    all: createList('all'),
    grammar: createList('grammar'),
    phrasalVerbs: createList('phrasalVerbs'),
    it: createList('it')
});

const blog = combineReducers({
    byId,
    idsByFilter
});

export default blog;