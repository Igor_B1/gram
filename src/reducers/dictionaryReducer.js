import { DICTIONARY } from "../actions/types";


export default (state = [], action) => {
    switch (action.type) {
        case DICTIONARY.GET:
            return [...state, ...action.payload];
        case DICTIONARY.ADD_WORD:
            return [...state, action.payload];
        case DICTIONARY.CLEAR_WORD:
            return action.payload;
        case DICTIONARY.REMOVE_WORD:
            return state.filter(item => item.id !== action.id);
        default:
            return state;
    }
}