import { LEARNED } from "../actions/types";

export default (state = [], action) => {
    switch (action.type) {
        case LEARNED.GET:
            return [...state, ...action.payload];
        case LEARNED.ADD:
            return [...state, action.payload];
        case LEARNED.REMOVE_WORD:
            return state.filter(item => item.id !== action.id);
        case LEARNED.CLEAR_WORD:
            return action.payload;
        default:
            return state;
    }
}