import { BLOG, GRAMMAR } from "../actions/types";

export const createList = filter => {
    return (state = [], action) => {
        switch (action.type) {
            case BLOG.GET:
            case GRAMMAR.GET:
                return action.filter === filter ? action.payload.result : state;
            case BLOG.ADD:
                return [...state, action.payload.result];
            default:
                return state;
        }
    };
};

