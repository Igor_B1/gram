import React from 'react';
import Loadable from "react-loadable";
import { Route } from "react-router-dom";

const LazyRoute = (props) => {
    const component = Loadable({
        loader: props.component,
        loading: () => <div className="main__block">Loading...</div>,
    });

    return <Route {...props} component={component} />;
};

export default LazyRoute;