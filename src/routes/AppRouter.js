import React, { Fragment } from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import GrammarRule from "../components/grammar/GrammarRule";
import BlogEdit from "../components/blog/BlogEdit";
import NotFound from "../components/NotFound";
import Header from "../components/header/Header";
import Footer from "../components/Footer";
import LazyRoute from "./LazyRoute";

const AppRouter = ({ location }) => (
    <Fragment>
        <Header/>
        <Switch>
            <LazyRoute path="/" component={() => import(/* webpackChunkName: "dashboard" */"../components/Dashboard")} exact/>
            <LazyRoute exact path="/grammar/:filter?" component={() => import(/* webpackChunkName: "grammar" */ "../components/grammar/Grammar")}/>
            <Route path="/grammar/:level/:id" component={ GrammarRule }/>
            <LazyRoute path="/dictionary" component={() => import(/* webpackChunkName: "dictionary" */ "../components/dictionary/Dictionary")}/>
            <Route path="/blog/edit/:id" component={ BlogEdit }/>
            <LazyRoute path="/blog/:filter?" component={() => import(/* webpackChunkName: "blog" */ "../components/blog/Blog")}/>
            <Route component={ NotFound }/>
        </Switch>
        {location.pathname === '/' ? null : <Footer/>}
    </Fragment>
);


export default withRouter(AppRouter);
