_Gram_ is a front-end application. It was developed to help people learn English faster. It has all the essential and clearly explained grammar rules, blog and vocabulary sections inside. 

**What were using**

* React 16
* Redux (+ Normalizr)
* React-dnd
* React-Router 4
* HTML5 + SCSS
* axios
* Webpack 4
* Jest, Enzyme.

######Note: Jest and Enzyme here will be tested after finishing the project (including backend). 

**To run**

You'll need to have git and node installed in your system.

Fork and clone the project:

``git clone git@bitbucket.org:Igor_B1/gram.git``

Then install the dependencies:

``npm install``
 
Build the project:

``npm run build``

In order to run the project open the first terminal and type

``npm run dev``
 
command, also in the second terminal type 

``npm run serve``


Json-server is using here as a temporary solution.
Gram will become a full-stack app (Node.js, Express and PostgreSQL) soon.