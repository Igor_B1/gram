const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const imageminMozjpeg = require('imagemin-mozjpeg');

const vendor = [
    "axios", "formik", "lodash", "moment",
    "normalizr", "react", "react-dnd", "react-dnd-touch-backend",
    "react-dom", "react-icons-kit", "react-loadable", "react-redux", "react-reveal", "react-router-dom",
    "redux", "redux-thunk", "uuid", "yup", "raf"
];


module.exports = {
    entry: {
        bundle: './src/app.js',
        vendor
    },
    output: {
        path: path.join(__dirname, 'public'),
        filename: '[name].js',
        chunkFilename: '[name]-[hash].js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.s?css$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {minimize: true}
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images/'
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }
        ]
    },
    optimization: {
        runtimeChunk: {name: 'manifest'},
        splitChunks: {
            cacheGroups: {
                commons: {
                    chunks: "initial",
                    minChunks: 2
                },
                vendor: {
                    test: "vendor",
                    name: "vendor",
                    chunks: "all",
                    priority: 4,
                    enforce: true
                }
            }
        },
        minimizer: [
            new TerserPlugin({parallel: true}),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),

        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html',
            minify: {
                html5: true,
                collapseWhitespace: true,
                keepClosingSlash: true,
                collapseInlineTagWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true
            }
        }),

        new CopyWebpackPlugin([
            {
                from: 'images',
                to: path.resolve(__dirname, 'public/images')
            }
        ]),
        new ImageminPlugin({
            plugins: [imageminMozjpeg({quality: 40})]
        }),

        // new CompressionPlugin({
        //     filename: '[path].gz[query]',
        //     algorithm: 'gzip',
        //     test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
        //     threshold: 8192,
        //     minRatio: 0.5
        // })
    ],
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        historyApiFallback: true,
        open: true,
        publicPath: '/'
    }
};

